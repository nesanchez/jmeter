import csv
import json
import yaml
import datetime
import time
from yarara_logger import YararaLogger, config_yarara_logger
from elasticsearch import Elasticsearch


class ElasticsearchJmeter:
    FIELD_NAMES_AGGREGATE = ("sampler_label", "aggregate_report_count", "average", "aggregate_report_median",
                             "aggregate_report_90%_line", "aggregate_report_min", "aggregate_report_max",
                             "aggregate_report_error%", "aggregate_report_rate", "aggregate_report_bandwidth",
                             "aggregate_report_stddev")
    FIELD_NAMES = ("timeStamp", "elapsed", "label", "responseCode", "responseMessage", "threadName",
                   "dataType", "success", "failureMessage", "bytes", "grpThreads", "allThreads", "Latency", "IdleTime")
    DOC_TYPE = "jmeter"

    DOC_TYPE_AGGREGATE = "jmeter_aggregate"

    def __init__(self, host='localhost', port=9200, index=""):
        self.host = host
        self.port = port
        self.es = Elasticsearch(["http://" + host + ":" + str(port)])
        self.index = index
        self.logger = config_yarara_logger()
        self.logger.info("Host: %s Port: %s Index: %s", self.host, self.port, self.index)

    def write_results_to_elasticsearch(self, files=dict(), unique_execute_id=None):
        self.logger.info("Starting writing results to elasticsearch. DocType: %s Files: %s",
                         self.DOC_TYPE, files)
        next_id = 1
        for alias, file in files.iteritems():
            self.logger.info("Writing file. Files: %s Alias: %s", file, alias)
            next_id = self.__write_result(alias, unique_execute_id, file, self.DOC_TYPE, next_id, self.FIELD_NAMES)

    def write_aggregate_result_to_elasticsearch(self, files=dict(), unique_execute_id=None):
        self.logger.info("Starting writing standard results to elasticsearch. DocType: %s Files: %s",
                         self.DOC_TYPE_AGGREGATE, files)
        next_id = 1
        for alias, file in files.iteritems():
            self.logger.info("Writing file. Files: %s Alias: %s", file, alias)
            next_id = self.__write_result(alias, unique_execute_id, file, self.DOC_TYPE_AGGREGATE, next_id,
                                          self.FIELD_NAMES_AGGREGATE)

    def __write_result(self, alias, unique_execute_id, file, doc_type=DOC_TYPE_AGGREGATE, id=0, fields=FIELD_NAMES_AGGREGATE):
        self.logger.info("Fields to read: s% ", fields)
        with open(file, 'rU') as csvfile:
            reader = csv.DictReader(csvfile, fields)
            result_list = list()
            for row in reader:
                row["Alias"] = alias
                row["unique_execute_id"] = unique_execute_id
                row["@timestamp"] = datetime.datetime.today().isoformat()
                result_list.append(row)
        self.logger.debug("ExecuteId %s - Reading csv result from jmeter: %s", unique_execute_id, result_list)
        for result in result_list:
            if result.get("sampler_label", '').lower() != "sampler_label" and \
                            result.get("sampler_label", '').lower() != "timeStamp":
                self.es.index(index=self.index, doc_type=doc_type, id=id, body=result)
                id += 1
        return id

    def get_error_percentage_by_alias_and_sampler(self, alias='', unique_execute_id='', sampler_label=''):
        response = 0
        body = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "unique_execute_id": unique_execute_id
                            }
                        },
                        {
                            "match": {
                                "sampler_label": sampler_label
                            }
                        },
                        {
                            "match": {
                                "Alias": alias
                            }
                        }
                    ]
                }
            }
        }
        self.logger.info("ExecuteId %s - Searching on elasticsearch. Query %s",
                         unique_execute_id, json.dumps(body, indent=4))
        hists_response = self.es.search(index=self.index, doc_type=self.DOC_TYPE_AGGREGATE, body=body)
        intent = 0
        while intent != 10 and len(hists_response['hits']['hits']) <= 0:
            self.logger.info("Waiting for get result from elasticsearch")
            time.sleep(30)
            hists_response = self.es.search(index=self.index, doc_type=self.DOC_TYPE_AGGREGATE, body=body)
            intent += 1

        self.logger.info("ExecuteId %s - Result from elasticsearch. Result %s",
                         unique_execute_id, json.dumps(hists_response['hits']['hits'], indent=4))

        if len(hists_response['hits']['hits']) > 0:
            for row in hists_response['hits']['hits']:
                if row['_source']['sampler_label'].lower() == sampler_label.lower():
                    response += float(row['_source']['aggregate_report_error%'].replace(',', '.').split('%')[0])

        self.logger.info("ExecuteId %s - Alias: %s Sampler: %s Percentage %s",
                         unique_execute_id, alias, sampler_label, response)
        return response

    def get_jmeter_results_from_elasticsearch_by_alias(self, alias, unique_execute_id):
        hists_response = self.__search_on_elastic_search(unique_execute_id, alias)
        return self.__calculate_average_jmeter(hists_response['hits']['hits'], alias, unique_execute_id, 'average')


    def get_jmeter_results_tps_from_elasticsearch_by_alias(self, alias, unique_execute_id):
        hists_response = self.__search_on_elastic_search(unique_execute_id, alias)
        return self.__calculate_average_jmeter(hists_response['hits']['hits'], alias, unique_execute_id,
                                               'aggregate_report_rate')

    def __search_on_elastic_search(self, unique_execute_id, alias):
        body = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "unique_execute_id": unique_execute_id
                            }
                        },
                        {
                            "match": {
                                "Alias": alias
                            }
                        }
                    ]
                }
            }
        }
        self.logger.info("ExecuteId %s - Searching on elasticsearch. Index: %s Alias: %s DocType: %s Query %s",
                         unique_execute_id, self.index, alias, self.DOC_TYPE_AGGREGATE, json.dumps(body, indent=4))
        hists_response = self.es.search(index=self.index, doc_type=self.DOC_TYPE_AGGREGATE, body=body)
        intent = 0

        while len(hists_response['hits']['hits']) <= 0 and intent != 10:
            self.logger.info("Waiting for get result from elasticsearch")
            time.sleep(30)
            hists_response = self.es.search(index=self.index, doc_type=self.DOC_TYPE_AGGREGATE, body=body)
            intent += 1

        return hists_response

    def __calculate_average_jmeter(self, hits_response, alias='', unique_execute_id=None, field_name=''):
        response = 0
        number_of_metric = 0
        total_percentage = 0
        for doc in hits_response:
            if str(doc['_source']['Alias']).lower() == alias.lower() and \
                            str(doc['_source']['sampler_label']).lower() != 'total':
                average = doc['_source'][field_name]
                if average != '':
                    total_percentage += float(average.replace(',', '.'))
                    number_of_metric += 1

        if number_of_metric > 0:
            response = round(total_percentage / number_of_metric, 1)
        self.logger.info("ExecuteId %s - Result: %s", unique_execute_id, response)
        return response

def config_elasticsearch_writer_from_file(config_file):
    with open(config_file, "r") as file:
        try:
            config = yaml.load(file)
        except yaml.YAMLError as exc:
            logger = config_yarara_logger()
            logger.error('An error occurred trying to get yaml configuration %s', exc.message)
            raise
    host = config.get('elasticsearch', {}).get('host', "")
    port = config.get('elasticsearch', {}).get('port', "")
    index = config.get('elasticsearch', {}).get('index', "")
    return ElasticsearchJmeter(host=host, port=port, index=index)