from elasticsearch import Elasticsearch
import yaml
import datetime
import json
from yarara_logger import YararaLogger, config_yarara_logger


class ElasticsearchMetricbeat:
    def __init__(self, url='localhost', port=9200, index='metricbeat'):
        self.url = url
        self.port = port
        self.index = index
        self.logger = config_yarara_logger()

    def get_result_by_metric_aggregation_metricbeat(self, host_name, metric, metric_fields, aggregation,
                                                    start_time_scenario, end_time_scenario):
        es = Elasticsearch("http://" + self.url + ':' + str(self.port))
        result = 0

        if aggregation.lower() == "average":
            diff_in_minute = int((end_time_scenario - start_time_scenario).total_seconds() / 60)
            must_array = [{
                "range": {
                    "@timestamp": {
                        "gte": 'now-' + str(diff_in_minute) + 'm',
                        "lt": 'now'
                    }
                }
            }, {
                "match": {
                    "metricset.name": metric.lower()
                }
            }
            ]
            body = {
                "query": {
                    "bool": {
                        "must": must_array
                    }
                }
            }
            self.logger.info("Metric: %s - Query to get metricbeat value: %s", metric, json.dumps(body, indent=4))
            res = es.search(index=self.index, doc_type="metricsets", body=body)
            self.logger.info("Metricbeat result: %s", json.dumps(res['hits']['total'], indent=4))
            result = self.__get_metric_average(res['hits']['hits'], host_name, metric_fields.split("."))

        elif aggregation.lower() == "unit":
            self.logger.info("End time scenario: %s", end_time_scenario)
            body = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "@timestamp": {
                                        "gte": 'now-2m',
                                        "lt": 'now-1m'
                                    }
                                }
                            },
                            {
                                "match": {
                                    "metricset.name": metric.lower()
                                }
                            }]
                    }
                }
            }
            self.logger.info("Query to get metricbeat value: %s", json.dumps(body, indent=4))
            res = es.search(index=self.index, doc_type="metricsets", body=body)
            self.logger.info("Metricbeat result: %s", json.dumps(res['hits']['total'], indent=4))
            result = self.__get_metric_unit(res['hits']['hits'], host_name, metric_fields.split("."))

        return result

    def __get_metric_unit(self, hists_response, host_name, json_fields):
        response = 0.0
        if len(hists_response) > 0:
            first_value_doc = hists_response[0]
            host = first_value_doc['_source']['beat']['hostname']
            if host.lower() == host_name.lower():
                response = first_value_doc["_source"][json_fields[0]][json_fields[1]][json_fields[2]][json_fields[3]]
        return response

    def __get_metric_average(self, hists_response, host_name, json_fields):
        response = 0.0
        number_of_metric = 0
        total_percentage = 0.0
        if len(json_fields) == 4:
            for doc in hists_response:
                host = doc['_source']['beat']['hostname']
                if host.lower() == host_name.lower():
                    number_of_metric += 1
                    total_percentage += doc['_source'][json_fields[0]][json_fields[1]][json_fields[2]][json_fields[3]]
        if number_of_metric > 0:
            response = round(total_percentage / number_of_metric, 1)
        return response


def config_elasticsearch_metricbeat_from_file(config_file):
    with open(config_file, "r") as file:
        try:
            config = yaml.load(file)
        except yaml.YAMLError as exc:
            print 'An error occurred trying to get yaml configuration ' + exc.message
            raise

    elastic_search_config = config.get('elasticsearch', {})
    return ElasticsearchMetricbeat(elastic_search_config.get('host'), elastic_search_config.get('port'),
                                   elastic_search_config.get('index'))
