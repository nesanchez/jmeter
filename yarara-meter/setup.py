from distutils.core import setup
#To install the library in your environment run python setup.py install
#To package the library for distribution (in zip format) run python setup.py sdist --format zip
setup(
    name="yarara-meter",
    version='0.0.1',
    py_modules=["yarara_jmeter" , "metricbeat_elasticsearch", "jmeter_elasticsearch", "yarara_logger"],

    #metadata
    author="IncluIT",
    description="Library to execute performance test in yarara using jmeter",
    license="Public content",
    keywords="yarara jmeter performance"
)
