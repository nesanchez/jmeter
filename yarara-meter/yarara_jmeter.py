from subprocess import check_call, CalledProcessError
import yaml
from multiprocessing import Process
from yarara_logger import YararaLogger, config_yarara_logger
import random

def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    if func_name.startswith('__') and not func_name.endswith('__'): #deal with mangled names
        cls_name = cls.__name__.lstrip('_')
        func_name = '_' + cls_name + func_name
    return _unpickle_method, (func_name, obj, cls)


def _unpickle_method(func_name, obj, cls):
    for cls in cls.__mro__:
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)


import copy_reg
import types
copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)


class JMeterConfig:

    def __init__(self, path='jmeter', host='127.0.0.1', port=80, remote=False, plans_dir="", aggregator_path=None,
                 unique_execute=None, results_dir="", global_vars={}):
        self.path = path
        self.aggregator_path = aggregator_path
        self.unique_execute = unique_execute
        self.host = host
        self.port = port
        self.remote = remote
        self.plans_dir = plans_dir
        self.results_dir = results_dir
        self.global_vars = global_vars
        self.test_processes = list()

def config_jmeter_from_file(config_file):
    with open(config_file, "r") as file:
        try:
            config = yaml.load(file)
        except yaml.YAMLError as exc:
            print 'An error occurred trying to get yaml configuration ' + exc.message
            raise
    config = config.get('jmeter', None)
    path = config.get('path', None)
    aggregator_path = config.get('aggregator_path', None)
    unique_execute = config.get('unique_execute', None)
    plans_dir = config.get('plans_dir', "")
    results_dir = config.get('results_dir', "")
    remote = config.get('remote', False)
    remote_host = config.get('remote_data', {}).get('hosts', None)
    remote_port = config.get('remote_data', {}).get('port', None)
    global_variables = dict(config.get('extra_variables', {}))
    return JMeterConfig(path=path, host=remote_host, port=remote_port, remote=remote,
                        plans_dir=plans_dir, aggregator_path=aggregator_path,
                        unique_execute=unique_execute, results_dir=results_dir, global_vars=global_variables)


def execute_test(jmeter_config, plans_and_vars=[]):
    for value in plans_and_vars:
        value = dict(value.items())
        variables = dict(jmeter_config.global_vars, **value.get("variables", {}))
        result_file = value.get("result", None)
        target_host = value.get("host", None)
        division = "/" if "/" in jmeter_config.results_dir else u"\\"
        result_path = jmeter_config.results_dir + division + result_file.split(".")[0] + ".jtl" if result_file is not None else None
        p = Process(target=run_single_test, args=(jmeter_config.plans_dir,
                                                  jmeter_config.remote, value.get("plan", ""),
                                                  jmeter_config.path, target_host, variables, result_path))
        jmeter_config.test_processes.append(p)
        p.start()


def wait_for_all_processes_end(jmeter_config):
    for process in jmeter_config.test_processes:
        process.join()

def wait_to_convert_jtl_to_cvs_aggregate_plugins(jmeter_configs_scenario, item_files):
    logger = config_yarara_logger()
    results_dict = dict()
    results_dir = jmeter_configs_scenario.results_dir
    division = "/" if "/" in results_dir else u"\\"
    cmd_runner_path = jmeter_configs_scenario.aggregator_path
    for key, test in item_files:
        result_csv = results_dir + division + test["result"].split(".")[0] + ".csv"
        input_jtl_report = results_dir + division + test["result"].split(".")[0] + ".jtl"
        results_dict[key] = result_csv
        cmd_command = "java -jar {cmd_runner_path} --tool Reporter --generate-csv {result_csv} "
        cmd_command += " --input-jtl  {input_jtl_report} --plugin-type AggregateReport"
        full_command = cmd_command.format(cmd_runner_path=cmd_runner_path, result_csv=result_csv,
                                          input_jtl_report=input_jtl_report)
        logger.info("Calling jmeter aggregate report plugin. Command: " + full_command)
        p = Process(target=execute_aggregate_report_jmeter, args=(full_command, True))
        p.start()
        p.join()

def execute_aggregate_report_jmeter(full_command, shell_value=True):
    check_call(full_command, shell=shell_value)

def run_single_test(plans_dir="", remote=False, plan="", path="", host="", variables={}, result_path=None):
    division = "/" if "/" in plans_dir else u"\\"
    plan_path = plans_dir + division + plan
    if not remote:
        run_local_test(path=path, jmeter_plan=plan_path, variables=variables, results_path=result_path)
    else:
        run_remote_test(path=path, host=host, jmeter_plan=plan_path, variables=variables, results_path=result_path)


def run_local_test(path, jmeter_plan=None, variables={}, results_path=None):
    logger = config_yarara_logger()
    command = "{jmeter_path} -n -t {jmeter_plan} {variables}"

    if results_path is not None:
        command += " -l {results_path}".format(results_path=results_path)
    try:
        command_to_execute = command.format(jmeter_path=path, jmeter_plan=jmeter_plan,
                                            variables=parse_variables_command("J", variables))
        logger.info("JMeter command to execute: %s", command_to_execute)
        check_call(command_to_execute, shell=True)
    except CalledProcessError as e:
        print "Some error occurred trying to execute jmeter command: " + e.message
        raise


def run_remote_test(path, host, jmeter_plan=None, variables={}, results_path=None):
    logger = config_yarara_logger()
    # variables["remote_hosts"] = host
    command = "{jmeter_path} -n -r -t {jmeter_plan} {variables} -Jremote_hosts=" + host

    if results_path is not None:
        command += " -l {results_path}".format(results_path=results_path)
    try:
        command_to_execute = command.format(jmeter_path=path, jmeter_plan=jmeter_plan,
                                  variables=parse_variables_command("G", variables))
        logger.info("JMeter command to execute: %s", command_to_execute)
        check_call(command_to_execute, shell=True)
    except CalledProcessError as e:
        print "Some error occurred trying to execute jmeter command: " + e.message
        raise


def parse_variables_command(flag, variables={}):
    variable_structure = " -" + flag + "{name}={value}"
    variables_command = ""
    for key, value in variables.items():
        variables_command += variable_structure.format(name=key.replace(" ", ""), value=value.replace(" ", ""))
    return variables_command