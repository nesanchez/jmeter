import logging

class YararaLogger:

    def __init__(self, name):
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.INFO)

    def getLogger(self):
        return self.logger

def config_yarara_logger():
    return YararaLogger('yarara-meter').getLogger()
