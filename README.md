# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
    - JMeter
    - Elasticsearch
    - CMDRunner jmeter plugins
    - Python
    - Python Behave

* How to run tests
In order to run a test you should follow these steps: 

1) Update or create a yarara-jmeter.yaml with this information:
  - jmeter:
      path: JMeter path bin example
          Example: 'path_to_jmeter_runner/jmeter'
      aggregator_path: Jar Path aggregate plugins(Cmd Runner)
          Example: 'path_to_jmeter_runner/cmdrunner-2.0.jar'
      plans_dir: Path about test plan jmx.
          Example: 'path_to_jmx/'
      results_dir: Dir path to allocate results 
      remote: no/yes => Indicate if there are jmeter remote host.
      extra_variables:
        host: domain/ip => app to run test.
        port: port_number => app port number.
   - elasticsearch:
      host: elastic search ip
      port: elastic search port
      index: index 
2) Update jmeter.feature according with test goals.

3) Execute run_scenario script(run_scenarios.bat or run_scenarios.sh)


* Deployment instructions

### How do I install Metricbeat ###

- Install Metricbeat as a service(Windows)
    1) Download the Metricbeat Windows zip file from the https://www.elastic.co/downloads/beats/metricbeat

    2) Extract the contents of the zip file into *C:\Program Files.*
    
    3) Rename the *metricbeat-<version>-windows* directory to *Metricbeat*.
    
    4) Open a PowerShell prompt as an Administrator (right-click the PowerShell icon and select Run As Administrator). If you are running Windows XP, you may need to download and install PowerShell. 
    
    5) Run the following commands to install Metricbeat as a Windows service:
        PS > cd 'C:\Program Files\Metricbeat'
        PS C:\Program Files\Metricbeat> .\install-service-metricbeat.ps1

    6) Execute the script start_metricbeat_service_cron.py. 
        **Example: python .\start_metricbeat_service_cron.py  folder_path_where_metricbeat_yml_file_is_allocated metricbeat_file_name_yml**
    
- Install Metricbeat as service(Linux)
    1) Download the last version of Metricbeat
        curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-5.0.0-amd64.deb

    2) Install Metricbeat using the file downloaded before.
        sudo dpkg -i metricbeat-5.0.0-amd64.deb

    3) Execute the script start_stop_metricbeat_service.sh. Example:
        **python .\start_metricbeat_service_cron.py folder_path_where_metricbeat_yml_file_is_allocated metricbeat_file_name_yml**
        
### How do I install Elasticsearch ###

    1) Download the last version of elasticsearch
       https://www.elastic.co/downloads/elasticsearch
    2) Unzip Elasticsearch
    3) Run bin/elasticsearch (or bin\elasticsearch.bat on Windows)
     

### How do I install jmeter plugins CMD ###
* We should follow this documentation:
    https://jmeter-plugins.org/wiki/JMeterPluginsCMD/

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact