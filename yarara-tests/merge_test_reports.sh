#!/bin/bash
export PATH="$HOME:$PATH"
source ~/virtualenvs/yarara/bin/activate
cd $(pwd)/project &> /dev/null
python merge_test_reports.py "$@"
cd .. &> /dev/null
deactivate
