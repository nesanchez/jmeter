@ECHO OFF
PUSHD %~dp0
set VIRTUALENV_FOLDER=\virtualenvs\yarara
cd deploy/virtualenv/
python create_bootstrap.py %* && (
	echo executed create_bootstrap
	REM rmdir /s /q %VIRTUALENV_FOLDER%
	python bootstrap.py %VIRTUALENV_FOLDER%
	cd ../../
) 
POPD