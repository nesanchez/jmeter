#!/bin/bash
cd  $(pwd)/deploy/virtualenv/ &> /dev/null
python create_bootstrap.py $@ && python bootstrap.py ~/virtualenvs/yarara/
cd ../../ &> /dev/null
