PUSHD %~dp0
call \virtualenvs\yarara\scripts\activate.bat
cd project
python merge_test_reports.py %*
cd ..
deactivate
POPD