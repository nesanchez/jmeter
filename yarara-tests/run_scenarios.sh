#!/bin/bash
export PATH="$HOME:$PATH"
source ~/virtualenvs/yarara/bin/activate
cd $(pwd)/../yarara-meter/
python setup.py install
cd -
cd $(pwd)/project &> /dev/null
python run_scenarios.py "$@" -c .\config\config.cfg
cd .. &> /dev/null
deactivate