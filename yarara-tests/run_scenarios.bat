PUSHD %~dp0
@ECHO OFF

call \virtualenvs\yarara\scripts\activate.bat
IF ERRORLEVEL 1 GOTO :EOF

cd ../yarara-meter
python setup.py install
cd ../yarara-tests/project

python run_scenarios.py %* -c .\config\config.cfg
cd ..

deactivate

POPD