"""
Entry point for running the test automation solution.
"""
import sys
from yarara.merge_reports import MergeReports

if __name__ == '__main__':
    merge = MergeReports()
    merge.merge_reports(sys.argv[1:])
