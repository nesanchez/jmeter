Feature: Sample feature to test the communication between Yarara and Jmeter

Background:
    Given a verified yarara-jmeter config file D:\development\yarara_meter\repo\jmeter\yarara-tests\project\config\yarara-jmeter.yaml

    @JMETER @FIRST_TESTS
    Scenario: Execute a sample performance test
        Given the scenario "test" composed by the following JMeter scripts
                    | JMeter            | Alias           | ResultFile        | TargetHost     |
                    | sample_test.jmx   | primer_plan     | primer_test_10     | 54.242.65.240  |
                    | sample_test2.jmx  | segundo_plan    | segundo_test_15    | 54.164.100.105 |
        And generate random unique execute test id
        And an aplication target for scenario "test" of:
                    | Alias            |  SamplerLabel     |  TPS   | Average Response Time    |
                    | segundo_plan     |   HTTP_Request    |  200   | 2000                     |
                    | primer_plan      |   HTTP_Request_2  |  250   | 2000                     |
        When the scenario "test" is executed with the following properties:
                    | Alias           | Property      | Value   |
                    | segundo_plan    | threads       | 5       |
                    | segundo_plan    | ramptime      | 1       |
                    | segundo_plan    | duration      | 600     |
                    | segundo_plan    | path          | /       |
                    | primer_plan     | threads       | 5       |
                    | primer_plan     | ramptime      | 1       |
                    | primer_plan     | duration      | 900     |
                    | primer_plan     | path          | /       |
        And wait until the scenario "test" is completed
        And the scenario "test" results are stored into "ElasticSearch"
        Then the following host kpis are retrieved and compared for "test" scenario:
            | Host             |  Metricset | Metric_Field                | Aggregation  | Comparision | Threshold  |
            | DESKTOP-L752NOM  |   CPU      | system.cpu.user.pct         | Average      | <           | 10         |
            | DESKTOP-L752NOM  |   MEMORY   | system.memory.used.pct      | Average      | <           | 10         |
            | DESKTOP-L752NOM  |   NETWORK  | system.network.out.bytes    | Average      | >           | 10000      |
            | DESKTOP-L752NOM  |   NETWORK  | system.network.in.bytes     | Average      | >           | 10000      |
            | DESKTOP-L752NOM  |   DISKIO   | system.diskio.read.bytes    | Average      | >           | 5000       |
            | DESKTOP-L752NOM  |   DISKIO   | system.diskio.write.bytes   | Average      | >           | 5000       |
            | DESKTOP-L752NOM  |   MEMORY   | system.memory.used.pct      | Unit         | <           | 20         |
        And the actual "test" scenario results does not differ from the target in more than "80" percentage
        And "segundo_plan" "HTTP_Request" error percentage for "test" scenario results does not exceed "50" percentage
        And "primer_plan" "HTTP_Request_2" error percentage for "test" scenario results does not exceed "50" percentage