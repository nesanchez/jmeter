from yarara import environment as yarara_env

def before_all(context):
    yarara_env.before_all(context)

def before_feature(context, feature):
    yarara_env.before_feature(context, feature)

def before_scenario(context, scenario):
    yarara_env.before_scenario(context, scenario)

def before_step(context, step):
    yarara_env.before_step(context, step)

def after_step(context, step):
    yarara_env.after_step(context, step)

def after_scenario(context, scenario):
    yarara_env.after_scenario(context, scenario)

def after_feature(context, feature):
    yarara_env.after_feature(context, feature)
        
def after_all(context):
    yarara_env.after_all(context)

