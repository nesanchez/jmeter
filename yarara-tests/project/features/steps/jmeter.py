from behave import given, when, then, step
from metricbeat_elasticsearch import ElasticsearchMetricbeat, config_elasticsearch_metricbeat_from_file
from yarara_jmeter import JMeterConfig, config_jmeter_from_file, execute_test, wait_for_all_processes_end, wait_to_convert_jtl_to_cvs_aggregate_plugins
from jmeter_elasticsearch import ElasticsearchJmeter, config_elasticsearch_writer_from_file
from yarara_logger import YararaLogger, config_yarara_logger
import datetime
import random
from subprocess import check_call, CalledProcessError

@given(u'a verified yarara-jmeter config file {config_file}')
def config_jmeter_with_file(context, config_file):
    logger = config_yarara_logger()
    logger.info("Starting configuration yarara meter yaml file from %s", config_file)
    full_config_file = config_file
    context.config_file = full_config_file
    context.jmeter_config = config_jmeter_from_file(full_config_file)
    context.elasticsearch_metricbeat = config_elasticsearch_metricbeat_from_file(full_config_file)
    context.elasticsearch_jmeter = config_elasticsearch_writer_from_file(full_config_file)
    logger.info("Leaving configuration yarara meter yaml file from %s", config_file)

@given(u'generate random unique execute test id')
def config_jmeter_with_file(context):
    logger = config_yarara_logger()
    context.unique_execute_id = random.randint(1, 1000000)
    logger.info("UNIQUE_EXECUTE_ID: %s", context.unique_execute_id)

@given(u'the scenario "{scenario_name}" composed by the following JMeter scripts')
def define_jmeter_plans(context, scenario_name):
    logger = config_yarara_logger()
    logger.info("Starting configuration JMeter scripts for %s scenario name", scenario_name)
    context.start_time_scenario = datetime.datetime.today()
    context.jmeter_tests = {}
    context.jmeter_configs = {}
    alias_plans = {}
    for row in context.table:
        alias_plans[row["Alias"]] = {}
        alias_plans[row["Alias"]]["plan"] = row["JMeter"]
        alias_plans[row["Alias"]]["result"] = row["ResultFile"] + str(random.randint(1, 1000000000))
        alias_plans[row["Alias"]]["host"] = row["TargetHost"]
    context.jmeter_tests[scenario_name] = alias_plans
    logger.info("Alias plan information: %s", alias_plans)
    context.jmeter_configs[scenario_name] = context.jmeter_config
    logger.info("Leaving configuration JMeter scripts for %s scenario name", scenario_name)

@given(u'an aplication target for scenario "{scenario_name}" of')
def define_application_targets(context, scenario_name):
    logger = config_yarara_logger()
    logger.info("Starting application target for scenario %s scenario name", scenario_name)
    for row in context.table:
        targets = {}
        targets["TPS"] = row["TPS"]
        targets["Average Response Time"] = row["Average Response Time"]
        targets["SamplerLabel"] = row["SamplerLabel"]
        context.jmeter_tests[scenario_name][row["Alias"]]["targets"] = targets
    logger.info("JMeter test information: %s", context.jmeter_tests[scenario_name])
    logger.info("Leaving application target for scenario %s scenario name", scenario_name)


@when(u'the scenario "{scenario_name}" is executed with the following properties')
def execute_jmeter_scenario(context, scenario_name):
    logger = config_yarara_logger()
    logger.info("Starting the properties for %s scenario name", scenario_name)
    for row in context.table:
        if context.jmeter_tests.get(scenario_name, {}).get(row["Alias"], {}).get("variables", None) is None:
            context.jmeter_tests[scenario_name][row["Alias"]]["variables"] = {}
        context.jmeter_tests[scenario_name][row["Alias"]]["variables"][row["Property"]] = row["Value"]
    logger.info("Starting test(s) execution...")
    execute_test(context.jmeter_configs[scenario_name], plans_and_vars=context.jmeter_tests[scenario_name].values())
    logger.info("Leaving executing stop for %s scenario name ", scenario_name)


@when(u'wait until the scenario "{scenario_name}" is completed')
def wait_for_finishing_tests(context, scenario_name):
    wait_for_all_processes_end(context.jmeter_configs[scenario_name])
    wait_to_convert_jtl_to_cvs_aggregate_plugins(context.jmeter_configs[scenario_name],
                                                 context.jmeter_tests[scenario_name].items())

@when(u'the scenario "{scenario_name}" results are stored into "{db_engine}"')
def save_jmeter_results(context, scenario_name, db_engine):
    logger = config_yarara_logger()
    logger.info("Starting storing result into Elasticsearch for %s scenario name", scenario_name)
    if db_engine.upper() == "ELASTICSEARCH":
        writer = config_elasticsearch_writer_from_file(context.config_file)
        results_dict = dict()
        results_dict_jtl = dict()
        results_dir = context.jmeter_configs[scenario_name].results_dir
        division = "/" if "/" in results_dir else u"\\"
        for key, test in context.jmeter_tests[scenario_name].items():
            result_csv = results_dir + division + test["result"].split(".")[0] + ".csv"
            result_jtl = results_dir + division + test["result"].split(".")[0] + ".jtl"
            results_dict[key] = result_csv
            results_dict_jtl[key] = result_jtl
        writer.write_aggregate_result_to_elasticsearch(files=results_dict,
                                                       unique_execute_id=context.unique_execute_id)
        #writer.write_results_to_elasticsearch(files=results_dict_jtl, unique_execute_id=context.unique_execute_id)

    else:
        logger.error("There is no engine support for %s", db_engine)
        assert False, "There is no engine support for %s" %db_engine
    logger.info("Leaving storing result into Elasticsearch for %s scenario name", scenario_name)


@then(u'the following host kpis are retrieved and compared for "{scenario_name}" scenario')
def execute_then_step(context, scenario_name):
    logger = config_yarara_logger()
    logger.info("Starting validating metricbeat step for %s scenario name", scenario_name)
    context.end_time_scenario = datetime.datetime.today()
    logger.info("End time scenario: %s", context.end_time_scenario)
    for row in context.table:
        host_name = row['Host']
        metricset = row['Metricset']
        metric_field = row['Metric_Field']
        aggregation = row['Aggregation']
        comparision = row['Comparision']
        threshold = row['Threshold']
        result_to_compare = context.elasticsearch_metricbeat.get_result_by_metric_aggregation_metricbeat(
            host_name, metricset, metric_field, aggregation, context.start_time_scenario, context.end_time_scenario)
        logger.info("Metricbeat validation. Host: %s Metricset: %s Aggregation: %s Comparision: %s "
                    "Threshold: %s Result from Elasticsearch: %s", host_name, metricset, aggregation,
                    comparision, threshold, result_to_compare)
        if "<" == comparision:
            msg = "The {} must be lest than {}".format(metric_field, threshold)
            assert float(result_to_compare) < float(threshold), msg
        elif ">" == comparision:
            msg = "The {} must be greater than {}".format(metric_field, threshold)
            assert float(result_to_compare) > float(threshold), msg
        elif "=" == comparision:
            msg = "The {} must be equals than {}".format(metric_field, threshold)
            assert float(result_to_compare) == float(threshold), msg
    logger.info("Leaving validating metricbeat step for %s scenario name", scenario_name)

@then(u'the actual "{scenario_name}" scenario results does not differ from the target in more than "{percentage_value}" percentage')
def execute_then_step(context, scenario_name, percentage_value):
    logger = config_yarara_logger()
    logger.info("Starting validating percentage value step for %s scenario name", scenario_name)
    for alias in context.jmeter_tests.get(scenario_name, []):
        scenario_dic = context.jmeter_tests[scenario_name]
        validate_average_response_time(context, context.unique_execute_id, scenario_dic, percentage_value, alias)
        validate_tps_response(context, context.unique_execute_id, scenario_dic, percentage_value, alias)

    logger.info("Leaving validating percentage value step for %s scenario name", scenario_name)

@then(u'"{alias_jmeter}" "{sampler_label}" error percentage for {scenario_name} scenario results does not exceed "{percentage_value}" percentage')
def execute_then_step(context, alias_jmeter, sampler_label, scenario_name, percentage_value):
    logger = config_yarara_logger()
    logger.info("Entering to error percentage step. Scenario: %s", scenario_name)
    unique_execute_id = context.unique_execute_id
    result = context.elasticsearch_jmeter.get_error_percentage_by_alias_and_sampler(alias_jmeter, unique_execute_id,
                                                                                    sampler_label)
    logger.info("Alias: %s Sampler: %s Percentage from elasticsearch: %s", alias_jmeter, sampler_label, result)
    msg = "The result exceed than {} percentage".format(percentage_value)
    assert float(result) <= float(percentage_value), msg
    logger.info("Leaving error percentage step")


def validate_average_response_time(context, unique_execute_id, scenario_dic, percentage_value, alias):
    logger = config_yarara_logger()
    average_response_time = scenario_dic.get(alias, {}).get("targets", {}).get("Average Response Time", 0)
    result = context.elasticsearch_jmeter.get_jmeter_results_from_elasticsearch_by_alias(alias, unique_execute_id)
    percentage_start = int(average_response_time) - ((int(percentage_value) * int(average_response_time)) / 100)
    percentage_limit = int(average_response_time) + ((int(percentage_value) * int(average_response_time)) / 100)

    logger.info("Alias: %s", alias)
    logger.info(
        "Average Time Targets Validation: Average to compare %s Percentage start: %s "
        "Percentage limit: %s Value from ES: %s", average_response_time, percentage_start, percentage_limit, result)
    msg = "The average response time result differ from the target in more than {} percentage".format(percentage_value)
    assert float(result) >= float(percentage_start) and float(result) <= float(percentage_limit), msg

def validate_tps_response(context, unique_execute_id, scenario_dic, percentage_value, alias):
    logger = config_yarara_logger()
    tps_response_time = scenario_dic.get(alias, {}).get("targets", {}).get("TPS", 0)
    percentage_start = int(tps_response_time) - ((int(percentage_value) * int(tps_response_time)) / 100)
    percentage_limit = int(tps_response_time) + ((int(percentage_value) * int(tps_response_time)) / 100)
    result = context.elasticsearch_jmeter.get_jmeter_results_tps_from_elasticsearch_by_alias(alias, unique_execute_id)
    logger.info("Alias: %s", alias)
    logger.info("TPS Targets Validation: Average to compare %s Percentage start: %s "
                "Percentage limit: %s Value from ES: %s", tps_response_time, percentage_start, percentage_limit, result)
    msg = "The TPS result differ from the target in more than {} percentage".format(percentage_value)
    assert float(result) >= float(percentage_start) and float(result) <= float(percentage_limit), msg
