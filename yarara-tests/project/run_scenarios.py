"""
Entry point for running the test automation solution.
"""
import sys
from yarara.runner import run_scenarios

if __name__ == '__main__':
    run_scenarios(sys.argv[1:])
