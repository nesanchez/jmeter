# -*- coding: utf-8 -*-
#from __future__ import unicode_literals
""" this module create file bootstrap.py
"""
import virtualenv, textwrap
import os
import ConfigParser
import argparse


CONFIG = ConfigParser.ConfigParser()
CONFIG.read(os.path.join("..", "..", "project", "config", "config.cfg"))
no_proxy = ""

def __parse_arguments():
    """This function parser arguments
    """
    parser = argparse.ArgumentParser(
        description='Yarara')
    parser.add_argument('--http_proxy',
                        default=False,
                        help='Indicate the proxy that should be used (e.g. '
                             '"http:\\proxy-us.intel.com:911"). \
                            In case no proxy should be used, please use the '
                             '--no_proxy argument',
                        required=False)
    parser.add_argument('--no_proxy',
                        action="store_true",
                        default=False,
                        help='Indicate that no proxy will be used for the '
                             'required operation',
                        required=False)
    args_parsed = parser.parse_args()
    if not args_parsed.no_proxy and not args_parsed.http_proxy:
        parser.error('\nPlease, indicate the proxy to be used or set the '
                     '--no_proxy argument. e.g: \n create_virtualenv.bat '
                     '--no_proxy \nOR \ncreate_virtualenv.bat '
                     '--http_proxy http://proxy-us.intel.com:911')
    else:
        http_proxy = args_parsed.http_proxy
        if args_parsed.no_proxy:
            http_proxy = ""
        create_bootstrap(http_proxy)
    no_proxy = os.environ.get('no_proxy')

def create_bootstrap(http_proxy):
    """Create file bootstrap """
    output = virtualenv.create_bootstrap_script(textwrap.dedent("""
    \"\"\"This module prepares the environment YARARA\"\"\"
    import os, subprocess, urllib2, shutil, platform
    from setuptools.command import easy_install
    from zipfile import ZipFile
    from platform import system
    import sys
    import ssl
    import zipfile

    cur_path = os.path.dirname(os.path.realpath(__file__))
    install_path = os.path.join(cur_path, "libs")   

    bin_folder = 'bin'
    if platform.system() == "Windows":
        bin_folder = 'Scripts'

    def after_install(options, virtualenv_dir):
        \"\"\"This function prepares the environment before installing YARARA
        \"\"\"
        set_proxies()
        install_python_packages(virtualenv_dir)
        pip = join(virtualenv_dir, bin_folder, 'pip')
        process = subprocess.Popen([pip, "freeze"], stdout=subprocess.PIPE)
        installed_libraries = process.communicate()[0].lower()

    def clean_proxies():
        \"\"\"Clean proxies for install yarara\"\"\"
        os.environ["http_proxy"] = ''
        os.environ["https_proxy"] = ''
        os.environ["no_proxy"] = 'github.intel.com'

    def set_proxies():
        \"\"\"Set proxies from environ\"\"\"
        os.environ["http_proxy"] = '""" + http_proxy + """'
        os.environ["https_proxy"] = '""" + http_proxy + """'
        os.environ["no_proxy"] = '"""+ no_proxy +"""'
        print "http_proxy: %s" % os.getenv("http_proxy")
        print "https_proxy: %s" % os.getenv("https_proxy")

    def install_python_packages(virtualenv_dir):
        \"\"\"Install package for yarara\"\"\"
        pip = join(virtualenv_dir, bin_folder, 'pip')
        install_requirements(virtualenv_dir, pip)

    def install_requirements(virtualenv_dir, pip):
        \"\"\"Install requirements for yarara\"\"\"
        home_reqs = join(virtualenv_dir, 'requirements.txt')
        shutil.copy('requirements.txt', home_reqs)
        with open(home_reqs,'r') as f:
            for requirement in f.readlines():
                requirement = requirement.rstrip(os.linesep)
                if not requirement:
                    continue
                print "-------------------------------------"
                print "Installing " + requirement
                print "-------------------------------------"
                if 'yarara' in requirement.partition('yarara'):
                    install_yarara(virtualenv_dir, 'install',
                    requirement.partition('==')[2].rstrip())
                else:
                    subprocess.call([pip, 'install', '--upgrade', requirement])


    def download_file(url, filename):
        \"\"\" function download file from url direction
        \"\"\"
        print "From: " + url
        print "To: " + filename
        if hasattr(ssl, '_create_unverified_context'):
            ssl._create_default_https_context = ssl._create_unverified_context
        else:
            ssl.CERT_REQUIRED = 0
        resp = urllib2.urlopen(url, timeout=200)
        dir_name = os.path.split(filename)[0]
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        with open(filename, 'wb') as output:
            output.write(resp.read())

    def install_yarara(virtualenv_dir, action="install", version=''):
        \"\"\"Install framework yarara\"\"\"
        print "Installing yarara from package.."
        if not version:
            version = 'latest'
        if system().upper() == 'WINDOWS':
            extension = 'zip'
        else:
            extension = 'tar.gz'
        install_file = os.path.join("..", "..", "yarara", "dist",
                                    "yarara-{}.{}".format(version, extension))
        install_file = os.path.abspath(install_file)
        if not os.path.exists(install_file):
            print "Download yarara-{}.{}".format(version, extension)
            clean_proxies()
            url = "https://github.intel.com/yarara/yarara-framework/archive/" +\
                  "{}.{}".format(version, extension)
            download_file(url, install_file)
            set_proxies()
            print 'Downloaded Yarara.'
        else:
            print 'Found yarara-{}.{}'.format(version, extension)
        subprocess.call([join(virtualenv_dir, bin_folder, 'pip'), 'install',
                        install_file])

    """))
    with open('bootstrap.py', 'w') as file_bootstrap:
        file_bootstrap.write(output)

if __name__ == '__main__':
    __parse_arguments()
