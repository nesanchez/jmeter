#We should use install-service-metricbeat.ps1 in order to create a metricbeat as a service.
#Also, we have to set the correct path about metricbeat.yml

Param(
  [string]$filePath
)
Write-debug $filePath
Stop-Service metricbeat
Copy-Item -Path $filePath -Destination 'C:\\Program Files\\Metricbeat\\metricbeat.yml'
Start-Service metricbeat