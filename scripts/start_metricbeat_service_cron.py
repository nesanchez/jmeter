#!/usr/bin/python

#args[0]: path file about where the metricbeat is allocated
#args[1]: file name to check if the value is changed or not
#Example: python .\start_metricbeat_service_cron.py D:\\development\\yarara_meter\\ metricbeat.yml

import time
import subprocess
import sys
import os
import platform
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class MetricbeatHandlerWatch(FileSystemEventHandler):
    print("Starting file watch handler!")

    def __init__(self, path_file, file_name):
        self.path_file = path_file
        self.file_name = file_name
        self.execute_metricbeat()

    def execute_metricbeat(self):
        if(platform.system() == 'Windows'):
            print("Calling stop/start metricbeat service from Windows.")
            full_path_metricbeat_service = os.path.abspath("start_stop_metricbeat_service.ps1 -filePath " +
                                                           self.path_file + self.file_name)
            p = subprocess.Popen(["powershell.exe", full_path_metricbeat_service], stdout=sys.stdout)
            p.communicate()
        else:
            print("Calling stop/start metricbeat service from Linux.")
            full_path_metricbeat_service = os.path.abspath("start_stop_metricbeat_service.sh " + self.path_file + "/" +
                                                           self.file_name)
            os.system("sh " + full_path_metricbeat_service)

    def process(self, event):
        print(event)
        if not event.is_directory and event.src_path.endswith(self.file_name):
            self.execute_metricbeat()

    def on_modified(self, event):
        self.process(event)

    def on_created(self, event):
        self.process(event)


if __name__ == "__main__":
    args = sys.argv[1:]
    event_handler = MetricbeatHandlerWatch(args[0], args[1])
    observer = Observer()
    observer.schedule(event_handler, path=args[0], recursive=False)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print("Error")
        observer.stop()

    observer.join()
