#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Please, provide metricbeat.yml file config"
else
    echo "Stopping metricbeat"
    sudo /etc/init.d/metricbeat stop
    echo "Replacing $1 file to /etc/metricbeat/ folder"
    sudo cp $1 /etc/metricbeat/metricbeat.yml
    echo "Starting metricbeat"
    sudo /etc/init.d/metricbeat start
fi
